

connect: 
	sudo minicom --device /dev/serial/by-id/usb-Sierra_Wireless__Incorporated_Sierra_Wireless_EM9293_EG3432512802B110-if03-port0

Options: 
	ctrl+a and then a letter (z for help)

Commands: 
	at!<COMMAND>?
		nrinfo 	- New Radio Info 
		gstatus - Operational info 
	AT!SELRAT=? - Benutzte technologie einstellen
		Wenn man einmal von automatic (00) runter ist, kann man da nich wieder drauf wechsen ohne das Modem neu zu verbinden  
	AT!BAND=?   - auch? 

Control Channels: 
	https://seafile.cloud.uni-hannover.de/d/8603bb201cc1423c9f58/files/?p=%2FQuectel_RG50xQ%26RM5xxQ_Series_AT_Commands_Manual_V1.1.1_Preliminary_20201009.pdf#%5B%7B%22num%22%3A208%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C51%2C707%2C0%5D
	https://seafile.cloud.uni-hannover.de/d/8603bb201cc1423c9f58/files/?p=%2FQuectel_RG50xQ%26RM5xxQ_Series_AT_Commands_Manual_V1.1.1_Preliminary_20201009.pdf#%5B%7B%22num%22%3A203%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C51%2C273%2C0%5D
	(https://seafile.cloud.uni-hannover.de/d/8603bb201cc1423c9f58/files/?p=%2FQuectel_RG50xQ%26RM5xxQ_Series_AT_Commands_Manual_V1.1.1_Preliminary_20201009.pdf#%5B%7B%22num%22%3A226%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C51%2C676%2C0%5D)
	
