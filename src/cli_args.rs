use clap::{
    builder::styling::{AnsiColor, Color, Style},
    Parser,
};

#[derive(Parser, Debug, Clone)]
#[command(version, styles=add_colors())]
pub struct Cli {
    /// If set, loads the specified file, creates a plot and quits
    #[clap(long, short, action, default_value = "")]
    pub load_file: String,

    /// The minimum difference in bandwidth to be considered a change
    #[clap(long, short, action, default_value_t = 0.0)]
    pub min_bw_diff: f64,

    /// If provided, wont open the bw-change server on port 9998 so that the serial-reader can run in standalone mode
    #[clap(long, short, action)]
    pub no_server: bool,

    /// If set, ignores commands from server 7777 and doesnt swap the 5g carrier on/off
    #[clap(long, short, action)]
    pub disable_bw_swap: bool,

    /// If set, wont include the temperature in the plot
    #[clap(long, short, action)]
    pub plot_no_temp: bool,

    /// If set, the svg-data will be cut off after the given time [s]
    #[clap(long, action, default_value_t = 200)]
    pub svg_data_cutoff_time: u64,
}

fn add_colors() -> clap::builder::Styles {
    clap::builder::Styles::styled()
        .literal(Style::new().fg_color(Some(Color::Ansi(AnsiColor::Green))))
}
