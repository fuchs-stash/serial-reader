use clap::Parser;
use plotters::style::{RGBColor, YELLOW};
use serialport::SerialPort;
use signal_hook::{consts::SIGINT, iterator::Signals};
use std::{
    fmt::Display,
    fs::{read_to_string, File},
    io::{Read, Write},
    net::TcpListener,
    sync::{atomic::AtomicBool, Arc},
    thread,
    time::{Duration, SystemTime, UNIX_EPOCH},
};

/*
ul: uplink
dl: downlink

LTE SCC{} bw

*/

pub mod cli_args;
pub mod plotter;
pub mod types;

use plotters::doc;
use types::*;

use crate::{cli_args::Cli, plotter::create_stacked_area_plot};

plotters::define_color!(PLOT_BLUE, 76, 114, 176, "");
plotters::define_color!(PLOT_ORANGE, 222, 132, 82, "");
plotters::define_color!(PLOT_GREEN, 86, 168, 104, "");
plotters::define_color!(PLOT_RED, 197, 78, 82, "");
plotters::define_color!(PLOT_PURPLE, 210, 60, 218, "");
plotters::define_color!(PLOT_BROWN, 147, 119, 97, "");

const SERIAL_PATH :&str = "/dev/serial/by-id/usb-Sierra_Wireless__Incorporated_Sierra_Wireless_EM9191_8W1331007402A125-if03-port0";
const EXPECTED_COMPONENTS: &[(&str, RGBColor)] = &[
    ("LTE_SCC4", PLOT_BROWN),
    ("LTE_SCC3", PLOT_PURPLE),
    ("LTE_SCC2", PLOT_RED),
    ("LTE_SCC1", PLOT_GREEN),
    ("LTE", PLOT_ORANGE),
    ("SCC2_NR5G", YELLOW),
    ("SCC1_NR5G", PLOT_BLUE),
];

const CMD_GSTATUS: &str = "at!gstatus?";
// const CMD_NRINFO: &str = "at!nrinfo?";
const CMD_SELRAT_BASE: &str = "at!selrat=";

#[derive(Clone, Copy)]
enum RatTechnologies {
    Automatic = 0,
    LTEOnly = 6,
    LTEAnd5G = 21,
}

impl RatTechnologies {
    fn cmd(&self) -> String {
        format!("{CMD_SELRAT_BASE}{}", *self as u16)
    }

    fn switch(&mut self) {
        match self {
            RatTechnologies::Automatic => *self = RatTechnologies::Automatic,

            RatTechnologies::LTEOnly => *self = RatTechnologies::LTEAnd5G,
            RatTechnologies::LTEAnd5G => *self = RatTechnologies::LTEOnly,
        }
    }
}

impl Display for RatTechnologies {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            Self::LTEAnd5G => "LTE And 5G",
            Self::LTEOnly => "LTE Only",
            Self::Automatic => "Automatic",
        };

        write!(f, "{s}")
    }
}

const EMPTY_BW: BandwidthComposition = BandwidthComposition::empty();

fn main() {
    let cli = Cli::parse();

    if !cli.load_file.is_empty() {
        println!("Creating Plot from file {}", cli.load_file);
        let cont = read_to_string(cli.load_file).unwrap();
        let history = parse_file_content(&cont);
        plotter::create_stacked_area_plot(
            "bw.svg",
            "Bandwidth",
            (1024, 512),
            history,
            cli.plot_no_temp,
            cli.svg_data_cutoff_time,
        );
        std::process::exit(0);
    }

    println!("Initializing...");

    let start_time = SystemTime::now().duration_since(UNIX_EPOCH).unwrap();

    let running = Arc::new(AtomicBool::new(true));
    let running_listener = running.clone();

    let mut signals = Signals::new(&[SIGINT]).unwrap();
    thread::spawn(move || {
        for sig in signals.forever() {
            running_listener.store(false, std::sync::atomic::Ordering::Relaxed);
            println!("Terminating...");
        }
    });

    // ctrlc::set_handler(move || {
    //     running_listener.store(false, std::sync::atomic::Ordering::Relaxed);
    //     println!("Terminating...");
    // })
    // .unwrap();

    let should_change_rat = Arc::new(AtomicBool::new(false));
    let should_change_rat_listener = should_change_rat.clone();

    std::thread::spawn(|| start_bw_swap_manager(should_change_rat_listener));

    // Change to LTEAnd5G for using the switching feature
    let mut current_rat = RatTechnologies::LTEAnd5G;

    let serial_builder = serialport::new(SERIAL_PATH, 115200)
        .stop_bits(serialport::StopBits::One)
        .parity(serialport::Parity::None)
        .timeout(Duration::from_millis(500))
        .flow_control(serialport::FlowControl::Hardware);
    let mut serial = serial_builder.open().unwrap();

    println!("Serial name: {}", serial.name().unwrap());
    println!("Serial Baud rate: {}", serial.baud_rate().unwrap());
    println!("Serial timeout: {:?}", serial.timeout());

    let mut history: Vec<BandwidthComposition> = vec![];
    let mut current_total = None;

    let bw_change_sock = if !cli.no_server {
        println!("Listening on 127.0.0.1:9998");
        let bw_change_sock = TcpListener::bind("0.0.0.0:9998").unwrap();
        Some(bw_change_sock.accept().unwrap().0)
    } else {
        None
    };

    if !cli.disable_bw_swap {
        println!("Setting initial Modem mode to {current_rat}...");
        execute_command(&mut serial, &current_rat.cmd(), 750).unwrap();
    }

    println!("Starting main loop...");
    while running.load(std::sync::atomic::Ordering::Relaxed) {
        if !cli.disable_bw_swap && should_change_rat.load(std::sync::atomic::Ordering::Relaxed) {
            should_change_rat.store(false, std::sync::atomic::Ordering::Relaxed);
            current_rat.switch();

            match execute_command(&mut serial, &current_rat.cmd(), 750) {
                Some(response) => println!("Changed RAT to {current_rat} with code {response}"),
                None => println!("Couldn't change RAT from {current_rat}"),
            }

            continue;
        }

        let content = execute_command(&mut serial, CMD_GSTATUS, 200);
        if content.is_none() {
            println!("Content None! Skipping...");
            continue;
        }

        let curr_time = SystemTime::now().duration_since(UNIX_EPOCH).unwrap();
        // let diff = curr_time - start_time;

        let mut bwc = BandwidthComposition::empty();
        bwc.time = curr_time;
        if let Some(cont) = content {
            bwc.parse_info(&cont);

            if let Some(old_sum) = current_total {
                let new_sum = bwc.bw_sum();
                let diff = new_sum - old_sum;
                current_total = Some(new_sum);

                if diff.abs() > cli.min_bw_diff {
                    // println!("Total BW: {new_sum}; Diff: {diff}");
                    if let Some(mut bw_change_sock) = bw_change_sock.as_ref() {
                        bw_change_sock
                            .write(format!("{:03}|{:03}|", new_sum as u8, old_sum as u8).as_bytes())
                            .unwrap();
                        // bw_change_sock
                        //     .write(&[new_sum as u8, old_sum as u8])
                        //     .unwrap();
                    }
                }
            } else {
                current_total = Some(bwc.bw_sum());
            }

            let old_bw_sum = history.last().unwrap_or(&EMPTY_BW).bw_sum();
            let mut print_change = true;
            if let Some(last_entry) = history.last() {
                print_change = bwc.bw_sum() != last_entry.bw_sum()
                    || bwc.tempertature != last_entry.tempertature
                    || bwc.thermal_mitigation_level != last_entry.thermal_mitigation_level;
            }
            if print_change {
                println!(
                    "[{:.2}]\tTotal-BW: {}\tLast-BW: {}\tDiff: {:02}\tTemp: {:02}\tTML: {}",
                    (curr_time - start_time).as_secs_f32(),
                    bwc.bw_sum(),
                    old_bw_sum,
                    bwc.bw_sum() - old_bw_sum,
                    bwc.tempertature,
                    bwc.thermal_mitigation_level
                );
            }

            history.push(bwc);
        }
    }

    println!("Saving data...");
    let mut outp_file = File::create("bw.log").unwrap();

    for elem in &history {
        outp_file
            .write_all(format!("{}\n", elem.save_format()).as_bytes())
            .unwrap();
    }

    println!("Creating Plot...");
    create_stacked_area_plot(
        "bw.svg",
        "Bandwidth",
        (1024, 512),
        history,
        cli.plot_no_temp,
        cli.svg_data_cutoff_time,
    );
}

fn execute_command(serial: &mut Box<dyn SerialPort>, cmd: &str, wait_time: u64) -> Option<String> {
    let mut buf = [0u8; 8196];
    let cmd = format!("{cmd}\r\n");
    serial.write_all(cmd.as_bytes()).unwrap();
    serial.flush().unwrap();
    std::thread::sleep(Duration::from_millis(wait_time));

    let res = serial.read(&mut buf);
    serial.clear(serialport::ClearBuffer::All).unwrap();

    if let Err(e) = res {
        println!("Read error: {e}");
        return None;
    }
    Some(String::from_utf8_lossy(&buf).to_string())
}

fn parse_file_content(content: &str) -> Vec<BandwidthComposition> {
    let mut history = vec![];

    for line in content.split('\n') {
        if line.is_empty() {
            continue;
        }
        let mut bwc = BandwidthComposition::empty();

        let mut line = line.split('§');
        let mut metadata = line.next().unwrap().split('|');
        let time = Duration::from_nanos(metadata.next().unwrap().parse::<u64>().unwrap());
        let thermal_mitigation_level = metadata.next().unwrap_or("0").parse::<u32>().unwrap();
        let tempertature = metadata.next().unwrap_or("0").parse::<u32>().unwrap();

        bwc.time = time;
        bwc.thermal_mitigation_level = thermal_mitigation_level;
        bwc.tempertature = tempertature;

        for elem in line.next().unwrap().split('|') {
            if elem.is_empty() {
                continue;
            }
            let mut split = elem.split(':');
            let comp = BandwidthComponent {
                name: split.next().unwrap().to_string(),
                state: ComponentState::from_str_repr(split.next().unwrap()),
                ul_configured: split.next().unwrap().parse().unwrap(),
                bw: split.next().unwrap().parse().unwrap(),
            };

            bwc.add_component(comp);
        }

        history.push(bwc);
    }
    history
}

/**
 * This Function is responsible for setting the provided switch ('should_change_rat') to true whenever
 * the bw-rat should change. How this is achieved (via tcp, timer or something else) can be changed
 */
fn start_bw_swap_manager(should_change_rat: Arc<AtomicBool>) -> ! {
    println!("Starting socket...");
    let sock = TcpListener::bind("0.0.0.0:7777").unwrap();
    let (mut sock, addr) = sock.accept().unwrap();

    println!("Accepted Connection from {addr}");

    let mut current_rat = RatTechnologies::LTEAnd5G;

    loop {
        let mut buf = [0u8; 256];
        sock.read(&mut buf).unwrap();

        let content = String::from_utf8_lossy(&buf);

        if content.contains("swap") {
            should_change_rat.store(true, std::sync::atomic::Ordering::Relaxed);
            current_rat.switch();
            let _ = sock.write(format!("Setting RAT to {current_rat}").as_bytes());
        }
    }
}
