use plotters::{
    backend::SVGBackend,
    chart::ChartBuilder,
    coord::{combinators::IntoLinspace, ranged1d::Ranged},
    drawing::IntoDrawingArea,
    element::{PathElement, Rectangle},
    series::{AreaSeries, LineSeries},
    style::{
        full_palette::ORANGE,
        text_anchor::{HPos, Pos, VPos},
        Color, IntoFont, IntoTextStyle, RGBColor, ShapeStyle, BLACK, RED, WHITE,
    },
};

use crate::{BandwidthComposition, ComponentState, EXPECTED_COMPONENTS};

/// The first data series of 'values' will be drawn at the top
pub fn create_stacked_area_plot(
    filename: impl Into<String>,
    title: impl Into<String>,
    img_size: (u32, u32),
    mut values: Vec<BandwidthComposition>,
    no_temp: bool,
    cutoff_time: u64,
) {
    // Convert to relative Times
    let start_time = values.first().unwrap().time;
    values.iter_mut().for_each(|e| {
        e.time = e.time - start_time;
    });

    let filename = filename.into();
    let title = title.into();

    let root_area = SVGBackend::new(&filename, img_size).into_drawing_area();

    let start_x = values.first().unwrap().time.as_millis();
    let end_x = cutoff_time as u128 * 1000; //values.last().unwrap().time.as_millis();

    let min_y: u64 = 0;

    let max_y: u64 = 200;

    root_area.fill(&WHITE).unwrap();

    let x_axis = (start_x..end_x).step(1);

    let mut cc = ChartBuilder::on(&root_area)
        .margin(5)
        .set_all_label_area_size(50)
        .caption(title, ("sans-serif", 40))
        .build_cartesian_2d(x_axis.range(), min_y..max_y)
        .unwrap();

    let y_label = if no_temp { "MHz" } else { "MHz / °C" };

    cc.configure_mesh()
        .x_labels(12)
        .y_labels(10)
        .x_desc("secs")
        // .y_desc(y_label)
        .axis_desc_style(("sans-serif", 27).into_font())
        .max_light_lines(10)
        .x_label_formatter(&|v| (*v / 1000).to_string())
        .y_label_formatter(&|v| v.to_string())
        .label_style(("sans-serif", 27).into_font())
        // .x_label_style(("sans-serif", 20).into_font())
        // .y_label_style(("sans-serif", 20).into_font())
        .draw()
        .unwrap();

    // left Label
    root_area
        .draw_text(
            y_label,
            &("sans-serif", 27)
                .into_font()
                .transform(plotters::style::FontTransform::Rotate270)
                .into(),
            (5, 255),
        )
        .unwrap();

    // Right Label
    root_area
        .draw_text(
            y_label,
            &("sans-serif", 27)
                .into_font()
                .transform(plotters::style::FontTransform::Rotate90)
                .into(),
            ((img_size.0 - 10) as i32, 210),
        )
        .unwrap();

    for i in 0..EXPECTED_COMPONENTS.len() {
        // Sum all future data points
        let base: Vec<(_, _)> = values
            .iter()
            .filter_map(|e| {
                if e.time.as_secs() > cutoff_time {
                    return None;
                }
                Some((
                    e.time.as_millis(),
                    e.bw_lte.iter().chain(&e.bw_5g).fold(0.0, |acc, elem| {
                        acc + if EXPECTED_COMPONENTS[i..]
                            .iter()
                            .map(|elem| elem.0)
                            .collect::<Vec<&str>>()
                            .contains(&elem.name.as_str())
                            && elem.state != ComponentState::Inactive
                            && !elem.ul_configured
                        {
                            elem.bw
                        } else {
                            0.0
                        }
                    }) as u64,
                ))
            })
            .collect();

        cc.draw_series(
            AreaSeries::new(
                base.iter().map(|elem| (elem.0, elem.1)),
                0,
                EXPECTED_COMPONENTS[i].1.mix(1.0),
            )
            .border_style(EXPECTED_COMPONENTS[i].1),
        )
        .unwrap()
        .label(EXPECTED_COMPONENTS[i].0)
        .legend(move |(x, y)| {
            Rectangle::new(
                [(x + 5, y - 5), (x + 20, y + 5)],
                EXPECTED_COMPONENTS[i].1.filled(),
            )
        });
    }

    if !no_temp {
        cc.draw_series(LineSeries::new(
            values.iter().filter_map(|data| {
                if data.time.as_secs() > cutoff_time {
                    return None;
                }
                Some((data.time.as_millis(), data.thermal_mitigation_level as u64))
            }),
            &ORANGE,
        ))
        .unwrap()
        .label("Thermal Mitigation Level")
        .legend(move |(x, y)| {
            PathElement::new(
                [(x + 5, y), (x + 20, y)],
                ShapeStyle::from(&ORANGE).filled(),
            )
        });

        cc.draw_series(LineSeries::new(
            values.iter().filter_map(|data| {
                if data.time.as_secs() > cutoff_time {
                    return None;
                }
                Some((data.time.as_millis(), data.tempertature as u64))
            }),
            &RED,
        ))
        .unwrap()
        .label("Temperature")
        .legend(move |(x, y)| {
            PathElement::new([(x + 5, y), (x + 20, y)], ShapeStyle::from(&RED).filled())
        });
    }

    cc.configure_series_labels()
        .label_font(("sans-serif", 27))
        .border_style(BLACK)
        .background_style(WHITE.mix(0.6))
        .position(plotters::chart::SeriesLabelPosition::UpperLeft)
        .draw()
        .unwrap();

    root_area.present().unwrap();
}
