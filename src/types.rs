use std::{collections::HashMap, time::Duration};

#[derive(Eq, PartialEq)]
pub enum ComponentState {
    Active,
    Inactive,
    Unknown,
}

impl ComponentState {
    pub fn from_str_repr(inp: &str) -> Self {
        match inp {
            "ACTIVE" => Self::Active,
            "INACTIVE" => Self::Inactive,
            _ => Self::Unknown,
        }
    }

    pub fn as_str(&self) -> &str {
        match self {
            Self::Active => "ACTIVE",
            Self::Inactive => "INACTIVE",
            Self::Unknown => "UNKNOWN",
        }
    }
}

pub struct BandwidthComponent {
    pub bw: f64,
    pub name: String,
    pub state: ComponentState,
    pub ul_configured: bool,
}

pub struct BandwidthComposition {
    pub time: Duration,
    pub thermal_mitigation_level: u32,
    pub tempertature: u32,
    pub bw_lte: Vec<BandwidthComponent>,
    pub bw_5g: Vec<BandwidthComponent>,
}

impl BandwidthComposition {
    pub const fn empty() -> Self {
        Self {
            time: Duration::ZERO,
            thermal_mitigation_level: 0,
            tempertature: 0,
            bw_lte: vec![],
            bw_5g: vec![],
        }
    }

    pub fn bw_sum(&self) -> f64 {
        self.bw_lte
            .iter()
            .chain(&self.bw_5g)
            .filter(|bwc| bwc.state != ComponentState::Inactive && !bwc.ul_configured)
            .map(|bwc| bwc.bw)
            .sum()
    }

    pub fn parse_info(&mut self, line: &str) {
        let map = convert_lines(line);

        let thermal_mitigation_level = map
            .get("Thermal_Mitigation_Level")
            .unwrap_or(&"0".to_string())
            .parse::<u32>()
            .unwrap_or(0);

        let temperature = map
            .get("Temperature")
            .unwrap_or(&"0".to_string())
            .parse::<u32>()
            .unwrap_or(0);

        self.thermal_mitigation_level = thermal_mitigation_level;
        self.tempertature = temperature;

        let extract_bw = |q_bw| {
            let bw = map
                .get(&q_bw)
                .unwrap_or(&"0 MHz".to_string())
                .replace("---", "0")
                .split(' ')
                .next()
                .unwrap()
                .parse::<f64>(); // apparently the bw can also be a comma-number (e.g. 1.4)

            match bw {
                Ok(val) => val,
                Err(e) => {
                    eprintln!("Failed parsing bw with error {e} on input '{}'", line);
                    0.0
                }
            }
        };

        let extract_bw_component = |name| {
            let q_bw = format!("{name}_bw");
            let q_state = format!("{name}_state");
            let q_ul_conf = format!("{name}_UL_Configured");
            let state = map.get(&q_state).unwrap_or(&"UNKNOWN".to_string()).clone();
            let uplink_configured = map.get(&q_ul_conf).unwrap_or(&"FALSE".to_string()).clone();
            let ul_configured = match uplink_configured.as_str() {
                "TRUE" => true,
                "FALSE" => false,
                _ => false,
            };

            let bw = extract_bw(q_bw);

            BandwidthComponent {
                bw,
                name,
                state: ComponentState::from_str_repr(&state),
                ul_configured,
            }
        };

        // LTE Main carrier
        {
            let name = "LTE".to_string();
            let q_bw = "LTE_bw".to_string();
            let bw = extract_bw(q_bw);

            self.bw_lte.push(BandwidthComponent {
                bw,
                name,
                state: ComponentState::Active,
                ul_configured: false,
            });
        }

        // LTE SCC{X}
        for i in 1..=4 {
            let name = format!("LTE_SCC{i}");

            let bwc = extract_bw_component(name);

            self.bw_lte.push(bwc)
        }

        // 5G Carrier
        for i in 1..=2 {
            let name = format!("SCC{i}_NR5G");

            let bwc = extract_bw_component(name);
            self.bw_5g.push(bwc)
        }
    }

    pub fn save_format(&self) -> String {
        let mut res = format!(
            "{}|{}|{}§",
            self.time.as_nanos(),
            self.thermal_mitigation_level,
            self.tempertature
        );

        for component in self.bw_lte.iter().chain(&self.bw_5g) {
            res = format!(
                "{res}{}:{}:{}:{}|",
                component.name,
                component.state.as_str(),
                component.ul_configured,
                component.bw
            )
        }

        res
    }

    pub fn add_component(&mut self, comp: BandwidthComponent) {
        if comp.name.contains("LTE") {
            self.bw_lte.push(comp);
        } else if comp.name.contains("5G") {
            self.bw_5g.push(comp);
        } else {
            eprintln!(
                "Provided BW Component doesnt contain 5G or LTE! ('{}', {})",
                comp.name, comp.bw
            );
        }
    }
}

fn convert_lines(inp: &str) -> HashMap<String, String> {
    let inp = inp.replace('\t', "\n");

    inp.split('\n')
        .filter(|line| !line.is_empty() && line.contains(':'))
        .map(|line| {
            line.split(':')
                .map(|elem| elem.trim_matches('\r').trim_matches(' '))
        })
        .map(|mut elems| {
            (
                elems.next().unwrap().replace(' ', "_").to_string(),
                elems.next().unwrap().to_string(),
            )
        })
        .collect()
}
